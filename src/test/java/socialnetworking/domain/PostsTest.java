package socialnetworking.domain;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import socialnetworking.domain.post.core.Post;
import socialnetworking.domain.post.core.Posts;

/**
 * Created by n07 on 12/01/17.
 */
public class PostsTest {

	@Test
	public void testPostInTimeline(){
		String message = "Mensaje";
		String username = "Bob";

		Post timelinePost = new Posts().post(username, message);
		Assert.assertTrue(timelinePost.postedBy(username));
	}

	@Test
	public void testReadTimeline() {
		String message = "Mensaje";
		String username = "Bob";

		Posts posts = new Posts();
		Post timelinePost = posts.post(username, message);

		List<Post> userTimeline = posts.read(username);
		Assert.assertTrue(userTimeline.contains(timelinePost));
	}
}
