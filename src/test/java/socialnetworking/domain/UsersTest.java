package socialnetworking.domain;

import org.junit.Assert;
import org.junit.Test;

import socialnetworking.domain.user.core.User;
import socialnetworking.domain.user.core.Users;

/**
 * Created by n07 on 12/01/17.
 */
public class UsersTest {

	@Test
	public void testFollowUser() {
		String username = "Bob";
		String following = "Alice";
		User user = new Users().follow(username, following);

		Assert.assertTrue(user.isFollowing(following));

	}
}
