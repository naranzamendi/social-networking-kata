package socialnetworking.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import socialnetworking.domain.post.core.Post;
import socialnetworking.domain.post.core.Posts;
import socialnetworking.domain.user.core.User;
import socialnetworking.domain.user.core.Users;

/**
 * Created by n07 on 12/01/17.
 */
public class WallServiceTest {

	@Test
	public void testReadWall(){
		Users users = new Users();
		Posts posts = new Posts();

		WallService wallService = new WallService(posts, users);

		posts.post("Alice", "It's a sunny day");
		posts.post("Bob", "Going to the central park");

		User alice = users.follow("Alice", "Bob");
		List<Post> wall = wallService.wall(alice.getUsername());

		Assert.assertEquals(2, wall.size());
	}


}
