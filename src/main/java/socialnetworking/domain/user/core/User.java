package socialnetworking.domain.user.core;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by n07 on 12/01/17.
 */
public class User {

	private String username;
	private List<String> following = new ArrayList<>();

	public User(String username) {
		this.username = username;
	}

	public boolean isFollowing(String username){
		return this.following.contains(username);
	}

	public boolean hasName(String username) {
		return this.username.equals(username);
	}

	public void follow(String username) {
		this.following.add(username);
	}

	public List<String> getFollowing() {
		return following;
	}

	public String getUsername() {
		return username;
	}
}
