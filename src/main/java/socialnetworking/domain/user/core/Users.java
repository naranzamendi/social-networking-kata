package socialnetworking.domain.user.core;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by n07 on 12/01/17.
 */
public class Users {

	List<User> users = new ArrayList<>();

	public User follow(String username, String following){
		User user = find(username);
		user.follow(following);
		return user;
	}

	public User find(String username) {
		return users.parallelStream().filter(user -> user.hasName(username))
							 .findFirst()
							 .orElse(create(username));
	}

	private User create(String username) {
		User user = new User(username);
		users.add(user);

		return user;
	}
}
