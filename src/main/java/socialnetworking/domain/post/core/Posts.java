package socialnetworking.domain.post.core;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by n07 on 12/01/17.
 */
public class Posts {

	private List<Post> posts = new ArrayList<>();

	public Posts() {
	}

	public Post post(String username, String message) {
		Post post = new Post(username, message, new Date());
		posts.add(post);
		return post;
	}

	public List<Post> read(String username) {
		return posts.parallelStream().filter(post -> post.postedBy(username))
									 .sorted((o1, o2) -> o2.getDate().compareTo(o1.getDate()))
				              		 .collect(Collectors.toList());
	}

	public List<Post> read(List<String> usernames){
		return usernames.parallelStream().flatMap(username -> read(username).stream())
										 .sorted((o1, o2) -> o2.getDate().compareTo(o1.getDate()))
										 .collect(Collectors.toList());
	}
}
