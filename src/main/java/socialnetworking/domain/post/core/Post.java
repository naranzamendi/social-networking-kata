package socialnetworking.domain.post.core;

import java.util.Date;

/**
 * Created by n07 on 12/01/17.
 */
public class Post {

	private String username;
	private String message;
	private Date date;

	public Post(String username, String message, Date date) {
		this.username = username;
		this.message = message;
		this.date = date;
	}

	public boolean postedBy(String username) {
		return this.username.equals(username);
	}

	public String getMessage() {
		return message;
	}

	public String getUsername() {
		return username;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Post post = (Post) o;

		if (username != null ? !username.equals(post.username) : post.username != null)
			return false;
		if (message != null ? !message.equals(post.message) : post.message != null)
			return false;
		return date != null ? date.equals(post.date) : post.date == null;
	}

	@Override
	public int hashCode() {
		int result = username != null ? username.hashCode() : 0;
		result = 31 * result + (message != null ? message.hashCode() : 0);
		result = 31 * result + (date != null ? date.hashCode() : 0);
		return result;
	}

	public Date getDate() {
		return date;
	}
}
