package socialnetworking.infrastructure;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import socialnetworking.domain.post.core.Post;

/**
 * Created by n07 on 12/01/17.
 */
public class PostFormatter {

	public String format(Post post){
		return new StringBuilder().append(post.getUsername())
								  .append(" - ")
								  .append(post.getMessage())
								  .toString();
	}

	public String format(List<Post> posts){
		return posts.stream().parallel().map(post -> format(post).concat(String.format(" (%s)", formatDate(post.getDate()))))
				.collect(Collectors.joining("\n"));
	}

	private String formatDate(Date post) {
		long secondsDiff = (new Date().getTime() - post.getTime()) / 1000;

		if(secondsDiff < 60)
			return secondsDiff + " second" + getPlural(secondsDiff) + " ago";
		else {
			long minutes = secondsDiff / 60;
			return minutes + " minute" + getPlural(minutes) + " ago";
		}

	}

	private String getPlural(long minutes) {
		return minutes > 1 ? "s" : "";
	}

}
