package socialnetworking;

import java.util.ArrayList;
import java.util.List;

import socialnetworking.command.Command;
import socialnetworking.command.FollowCommand;
import socialnetworking.command.PostCommand;
import socialnetworking.command.ReadCommand;
import socialnetworking.command.WallCommand;
import socialnetworking.domain.post.core.Posts;
import socialnetworking.domain.user.core.Users;
import socialnetworking.service.WallService;

/**
 * Created by n07 on 12/01/17.
 */
public class SocialNetworking {

	public static final void main(String[] args){

		Users users = new Users();
		Posts posts = new Posts();

		WallService wallService = new WallService(posts, users);
		List<Command> commands = new ArrayList<>();

		commands.add(new PostCommand(posts));
		commands.add(new FollowCommand(users));
		commands.add(new WallCommand(wallService));
		commands.add(new ReadCommand(posts));

		String input = "";
		prompt();
		while((input = System.console().readLine()) != "quit\n"){
			String finalInput = input;
			String response = commands.stream().filter(command -> command.applies(finalInput))
					                           .findFirst()
										       .map(command -> command.execute(finalInput))
											   .orElse("");
			System.out.println(response);
			prompt();
		}

	}

	private static void prompt() {
		System.out.print("<social-networking>:");
	}

}
