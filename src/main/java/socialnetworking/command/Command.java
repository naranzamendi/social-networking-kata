package socialnetworking.command;

/**
 * Created by n07 on 12/01/17.
 */
public interface Command {

	String execute(String command);

	boolean applies(String command);
}
