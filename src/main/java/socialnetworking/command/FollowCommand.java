package socialnetworking.command;

import socialnetworking.domain.user.core.Users;

/**
 * Created by n07 on 12/01/17.
 */
public class FollowCommand implements Command {

	private Users users;

	String prefix = " follows ";

	public FollowCommand(Users users) {
		this.users = users;
	}

	@Override
	public String execute(String command) {
		String[] args = command.split(prefix);
		users.follow(args[0], args[1]);
		return "";
	}

	@Override
	public boolean applies(String command) {
		return command.contains(prefix);
	}
}
