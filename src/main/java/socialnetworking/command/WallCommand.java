package socialnetworking.command;

import java.util.List;

import socialnetworking.domain.post.core.Post;
import socialnetworking.infrastructure.PostFormatter;
import socialnetworking.service.WallService;

/**
 * Created by n07 on 12/01/17.
 */
public class WallCommand implements Command{

	private WallService wallService;

	private String suffix = " wall";

	public WallCommand(WallService wallService) {
		this.wallService = wallService;
	}

	@Override
	public String execute(String command) {
		String username = command.replace(suffix, "");
		List<Post> posts = wallService.wall(username);
		return new PostFormatter().format(posts);
	}

	@Override
	public boolean applies(String command) {
		return command.contains(suffix);
	}
}
