package socialnetworking.command;

import java.util.List;

import socialnetworking.domain.post.core.Post;
import socialnetworking.domain.post.core.Posts;
import socialnetworking.infrastructure.PostFormatter;

/**
 * Created by n07 on 12/01/17.
 */
public class ReadCommand implements Command {

	private Posts posts = new Posts();

	public ReadCommand(Posts posts) {
		this.posts = posts;
	}

	@Override
	public String execute(String command) {
		List<Post> posts = this.posts.read(command);
		return new PostFormatter().format(posts);
	}

	@Override
	public boolean applies(String command) {
		return true;
	}
}
