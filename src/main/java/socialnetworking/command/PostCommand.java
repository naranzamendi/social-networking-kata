package socialnetworking.command;

import socialnetworking.domain.post.core.Post;
import socialnetworking.domain.post.core.Posts;
import socialnetworking.infrastructure.PostFormatter;

/**
 * Created by n07 on 12/01/17.
 */
public class PostCommand implements Command {

	private Posts posts;
	private String separator = " -> ";

	public PostCommand(Posts posts) {
		this.posts = posts;
	}

	@Override
	public String execute(String command) {
		String[] args = command.split(separator);
		Post post = posts.post(args[0], args[1]);
		return new PostFormatter().format(post);
	}
	@Override
	public boolean applies(String command) {
		return command.contains(separator);
	}

}
