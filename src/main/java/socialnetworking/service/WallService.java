package socialnetworking.service;

import java.util.ArrayList;
import java.util.List;

import socialnetworking.domain.post.core.Post;
import socialnetworking.domain.post.core.Posts;
import socialnetworking.domain.user.core.User;
import socialnetworking.domain.user.core.Users;

/**
 * Created by n07 on 12/01/17.
 */
public class WallService {

	private Posts posts;
	private Users users;

	public WallService(Posts posts, Users users) {
		this.posts = posts;
		this.users = users;
	}

	public List<Post> wall(String username){

		User user = users.find(username);
		ArrayList<String> usernames = new ArrayList<>(user.getFollowing());
		usernames.add(user.getUsername());
		return posts.read(usernames);
	}
}
