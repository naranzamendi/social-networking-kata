Social Networking Kata
----------------------

Implement a social networking application (similar to Twitter) satisfying the scenarios below.

### Scenarios

#### Iteration 1:
**Posting**: Alice can publish messages to a personal timeline

> \> Alice -> I love the weather today

#### Iteration 2:
**Reading**: Bob can view Alice’s timeline

> \> Alice

> \> I love the weather today (5 minutes ago)

#### Iteration 3:
**Following**: Charlie can subscribe to Alice’s and Bob’s timelines, and view an aggregated list of all subscriptions

> \> Charlie -> I'm in New York today! Anyone wants to have a coffee?

> \> Charlie follows Alice

#### Iteration 4:
**Wall**: Charlie view an aggregated list of all subscriptions
> \> Charlie wall

> \> Charlie - I'm in New York today! Anyone wants to have a coffee? (15 seconds ago)

> \> Alice - I love the weather today (5 minutes ago)

- Don't worry about handling any exceptions or invalid commands. Assume that the user will always type the correct commands. Just focus on the sunny day scenarios